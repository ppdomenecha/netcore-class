﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using API.Models;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Services;
using Services.Models;

namespace API.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class WeatherForecastController : ControllerBase
    {
        private static readonly string[] Summaries = new[]
        {
            "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
        };

        private readonly ILogger<WeatherForecastController> _logger;
        private readonly IMapper mapper;
        private readonly TestService testService;

        public WeatherForecastController(ILogger<WeatherForecastController> logger, IMapper mapper, TestService testService)
        {
            _logger = logger;
            this.mapper = mapper;
            this.testService = testService;
        }

        [HttpGet]
        public IEnumerable<WeatherForecastDto> Get()
        {
            return GetRandomWeatherForecastsDtos();
        }

        [HttpPost]
        public void Post()
        {
            var list = GetRandomWeatherForecastsDtos();

            var listSvcModel = this.mapper.Map<WeatherForecastSvcModel[]>(list);
            this.testService.InsertMultipleWeatherForecasts(listSvcModel);
        }

        private static WeatherForecastDto[] GetRandomWeatherForecastsDtos()
        {
            var rng = new Random();

            return Enumerable.Range(1, 5).Select(index => new WeatherForecastDto
            {
                Date = DateTime.Now.AddDays(index),
                TemperatureC = rng.Next(-20, 55),
                Summary = Summaries[rng.Next(Summaries.Length)]
            })
                        .ToArray();
        }

        [HttpGet("~/test")]
        public WeatherForecastDto Test()
        {
            WeatherForecastSvcModel result = this.testService.GetWeatherForecast();

            var resultDto = this.mapper.Map<WeatherForecastDto>(result);

            return resultDto;
        }
    }
}
