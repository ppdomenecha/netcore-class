﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Services.Models
{
    public class WeatherForecastSvcModel
    {
        public Guid Id { get; set; }
        public DateTime Date { get; set; }

        public int TemperatureC { get; set; }

        public int TemperatureF => 32 + (int)(TemperatureC / 0.5556);

        public string Summary { get; set; }

        public DateTime CreatedAt { get; set; }
    }
}
