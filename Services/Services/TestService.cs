﻿using AutoMapper;
using Entities.Context;
using Services.Models;
using Shopping.Models;
using System;

namespace Services
{
    public class TestService
    {
        private readonly ShoppingContext context;
        private readonly IMapper mapper;

        public TestService(ShoppingContext context, IMapper mapper)
        {
            this.context = context;
            this.mapper = mapper;
        }

        public WeatherForecastSvcModel GetWeatherForecast()
        {
            WeatherForecast result = this.context.WeatherForecasts.Find(new Guid("14a03e93-b2ba-4145-b12d-17dde58aefeb"));

            var resultSvc = this.mapper.Map<WeatherForecastSvcModel>(result);
            return resultSvc;
        }

        public void InsertMultipleWeatherForecasts(WeatherForecastSvcModel[] list)
        {
            throw new NotImplementedException();
        }
    }
}
